#include <gtk/gtk.h>


void
on_button_sync_clicked                 (GtkButton       *button,
                                        gpointer         user_data);

void
on_button_new_folder_clicked           (GtkButton       *button,
                                        gpointer         user_data);

void
on_button_delete_clicked               (GtkButton       *button,
                                        gpointer         user_data);

void
on_button4_clicked                     (GtkButton       *button,
                                        gpointer         user_data);

void
on_window_rio_realize                  (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_window_rio_destroy                  (GtkObject       *object,
                                        gpointer         user_data);

