#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"
#include "rio500_api.h"

#include "../pixmaps/folder.xpm"
#include "../pixmaps/ofolder.xpm"

Rio500 *rio = NULL;
void add_songs_to_folder (GtkCTree *ctree, GtkCTreeNode *parent, GList *songs);

void
on_button_sync_clicked                 (GtkButton       *button,
                                        gpointer         user_data)
{
  GList *item, *content = rio_get_content (rio);
  GtkCTree *ctree = (GtkCTree*) lookup_widget (button, "ctree_content");
  GtkCTreeNode *sibling;
  GtkWidget *window = (GtkWidget*)lookup_widget (button, "window_rio");
  RioFolderEntry *folder;
  char *column[3];
  GdkColor transparent = { 0 };
  GdkPixmap *folder_pixmap, *ofolder_pixmap;
  GdkBitmap *folder_mask, *ofolder_mask;

  folder_pixmap = gdk_pixmap_create_from_xpm_d (window->window, &folder_mask,
                                                &transparent, folder_xpm);
  ofolder_pixmap = gdk_pixmap_create_from_xpm_d (window->window, &ofolder_mask,
                                                &transparent, ofolder_xpm);



  /* Remove old content */
  gtk_ctree_remove_node (ctree, NULL);

  /* Read content from rio and display add it to tree */
  sibling = NULL;
  for (item = g_list_first (content); item; item = item->next)
  {
    folder = (RioFolderEntry *)item->data;
    column[1] = folder->name; 
    column[0] = "";
    column[2] = "";
    sibling = gtk_ctree_insert_node ( ctree, NULL, NULL, column, 5,
                                      folder_pixmap, folder_mask,
                                      ofolder_pixmap, ofolder_mask, 
                                      FALSE, FALSE);
    add_songs_to_folder (ctree, sibling, folder->songs);
  }

  /* Free memory used by content info */
  rio_destroy_content (content);

}

void
add_songs_to_folder (GtkCTree *ctree, GtkCTreeNode *parent, GList *songs)
{
  GList *item;
  GtkCTreeNode *sibling;
  RioSongEntry *song;
  char *column[3], buffer[20];

  /* Read content from rio and display add it to tree */
  sibling = NULL;
  for (item = g_list_first (songs); item; item = item->next)
  {
    song = (RioSongEntry *)item->data;
    column[0] = "";
    column[1] = song->name;
    sprintf (buffer, "%d Kb", song->size/1024);
    column[2] = buffer;
    sibling = gtk_ctree_insert_node (ctree, parent, NULL, column, 5,
                                      NULL, NULL, NULL, NULL, FALSE, FALSE);
  }
}


void
on_button_new_folder_clicked           (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_button_delete_clicked               (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_button4_clicked                     (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_window_rio_realize                  (GtkWidget       *widget,
                                        gpointer         user_data)
{
  /* Create rio connection */
  if (rio == NULL)
  {
    rio = rio_new ();
  }
}

void
on_window_rio_destroy                  (GtkObject       *object,
                                        gpointer         user_data)
{
  if (rio != NULL)
    rio_delete (rio);
  gtk_widget_destroy ((GtkWidget*)object);
  gtk_exit(0);
}

